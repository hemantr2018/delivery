import React, { Component } from 'react';
import './../css/createroster.css';

class RoasterDetails extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
           <React.Fragment>
              <section style={{"margin-top":"3%", "display":"block"}} className="output">
                    <div className="container">
                      <div className="table-responsive">
                    <table className="table table-striped">
                      <thead>
                          <tr>
                              <th>Delivery Agent Id</th>
                              <th>Name</th>
                              <th>Order Id</th>
                              <th>Sequence No</th>
                              <th>Total Straight Line Distance</th>
                              <th>Total Distance</th>
                            </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td>11</td>
                        <td>Shyam</td>
                            <td>10<br/>12<br/>15<br/>18<br/>19</td>
                            <td>1<br/>2<br/>3<br/>4<br/>5</td>
                            <td>75</td>
                            <td>85</td>
                        <td className="editButton">View Route</td>
                      </tr>
                      <tr>
                        <td>12</td>
                        <td>Gaurav</td>
                        <td>13<br/>89<br/>98</td>
                        <td>1<br/>2<br/>3</td>
                        <td>82</td>
                        <td>95</td>
                        <td className="editButton">View Route</td>
                      </tr>
                    </tbody>
                    </table>
                  </div>
                  </div>
                  </section>
                  <section className="outputVisualizationCard outputvisualization">
                      <div className="container">
                          <div className="row">
                              <div className="col-8 col-lg-8 col-sm-8 col-md-8 col-xs-8"></div>
                              <div className="col-3 col-lg-3 col-sm-3 col-md-3 col-xs-3">
                                    <div className="search">
                                            <i className="fa fa-search"></i>
                                            <input type="text" className="outputVisualizationSearch" placeholder="Search By Agent Id"/>
                                          </div>
                              </div>
                              <div className="col-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                          </div>
                          <div className="row outputVisualizationMap">
                          <div className="col-12 col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div className="mapouter"><div className="gmap_canvas"><iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=athmin%20nodia&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
                            {/*<style>.mapouter{position:relative;text-align:right;height:300px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:100%;}</style>*/}
                            </div>
                              </div>
                            </div>
                      </div>
                  </section>
         </React.Fragment>
        )
    }
}

export default RoasterDetails
