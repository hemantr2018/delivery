import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './../css/cities.css';
import { connect } from "react-redux";

import { fetch_city } from "./../js/apis/citieslist";
import { create_roaster } from "./../js/apis/createRoaster";
import { get_all_roaster } from "./../js/apis/getAllRoasters";
import { get_grids } from "./../js/apis/getGrids";
import { get_roaster_orders } from "./../js/apis/getRoasterOrders";
import { create_roaster_orders } from "./../js/apis/createRoasterOrders";
import { get_roaster_agents } from "./../js/apis/getRoasterAgents";
import { create_roaster_agents } from "./../js/apis/createRoasterAgents";
import { user_login } from "./../js/apis/login";


class CitiesCmp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            //"articles":[
            //{ title: "React Redux Tutorial for Beginners", id: 1 },
            //{ title: "TypeScript tutorial for beginners", id: 2 }
        //]
			citi_list: [],
			city_name_filter: ""
        }
    }

    searchCity = function (event) {
		this.setState({city_name_filter: event.target.value.toLowerCase() });
        console.log("In searchCity");
    }

    editCity = function (city_id) {
        console.log("In editCity");
    }
    
    async componentDidMount() {
        console.log("In componentDidMount");
        //await user_login();
        let city_info = await fetch_city();
        console.log("city_info : ",city_info);
        this.setState({"citi_list": city_info});
        /*
        fetch_city();
        create_roaster();
        get_all_roaster();
        get_grids();
        
        get_roaster_orders();
        create_roaster_orders();
        get_roaster_agents();
        create_roaster_agents();*/
    }
    

    render() {
        //const { articles } = this.state;
        //console.log(this.state.articles);
        //return <ul>{articles.map(el => <li key={el.id}>{el.title}</li>)}</ul>;
        return (
           <React.Fragment>
           <section className="headingRow">
                <div className="container">
                  <div className="row">
                    <div className="col-md-1 col-xs-1 col-sm-1 col-1 col-lg-1">
                      <p className="citiesHeading">Cities</p>
                      <p className="citiesHeadingLine"></p>
                    </div>
                    <div className="col-md-6 col-xs-6 col-sm-6 col-6 col-lg-6"></div>
                    <div className="col-md-3 col-xs-3 col-sm-3 col-3 col-lg-3">
                    <div className="search">
                        <i className="fa fa-search"></i>
                        <input type="text" className="citiesSearch" placeholder="Search" onChange={this.searchCity.bind(this)}/>
                      </div>
                    </div>
                    <div className="col-md-2 col-xs-2 col-sm-2 col-2 col-lg-2">
                        <Link to="/map-roaster"><button type="button" className="btn addCitiesBtn btn-sm">Add new  <i className="fa fa-plus"></i></button></Link>
                      </div>
                  </div>
                </div>
              </section>
              <section>
                <div className="container">
                  <div className="table-responsive">
                <table className="table table-striped">
                  <thead>
                      <tr>
                          <th>Country</th>
                          <th>City</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                  </thead>
                  <tbody>
                  {this.state.citi_list.map((city_obj, index) => (
					  (this.state.city_name_filter.length == 0 || (city_obj.name.toLowerCase()).indexOf(this.state.city_name_filter) != -1) && 
						  <tr>
							<td>India</td>
							<td>{city_obj.name}</td>
							<td>Active</td>
							<td className="editButton" onClick={this.editCity.bind(this, city_obj.id)}><i className="fa fa-pencil"></i>Edit</td>
						  </tr>
                  ))
                  }
                </tbody>
                </table>
              </div>
              </div>
              </section>
         </React.Fragment>
        )
    }
}

function mapStateToProps(state){
  return { articles: state.articles };
}

export default connect(mapStateToProps)(CitiesCmp)
