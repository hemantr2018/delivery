import React, { Component } from 'react';
import './../css/createcities.css';
import { Link } from 'react-router-dom';


class MapRoaster extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
           <React.Fragment>
           <section>
                <div className="container">
                  <div className="row">
                    <div className="col-md-3 col-xs-3 col-sm-3 col-3 col-lg-3">
                      <p className="createCitiesHeading">Create New</p>
                      <p className="createCitiesHeadingLine"></p>
                    </div>
                    <div className="col-md-7 col-xs-7 col-sm-7 col-7 col-lg-7"></div>
                    <div className="col-md-2 col-xs-2 col-sm-2 col-2 col-lg-2">
                        <Link to="/cities"><button type="button" className="btn addCitiesBtn btn-sm">Back</button></Link>
                    </div>
                  </div>
                </div>
              </section>

              <section className="createCitiesBox">
                  <div className="container">
                      <div className="row citiesInputAlignment">
                          <div className="col-3 col-md-3 col-lg-3 col-sm-3 col-xs-3">
                              <label className="labelHeading">Country</label><br/>
                              <input className="createCitiesInput" type="text" placeholder="Country" />
                          </div>
                          <div className="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                              <label className="labelHeading">City</label><br/>
                              <input className="createCitiesInput" type="text" placeholder="City" />
                          </div>
                          <div className="col-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                          <div className="col-2 col-md-2 col-lg-2 col-sm-2 col-xs-2">
                              <label className="labelHeading">Active</label><br/>
                              <label className="radio-inline">
                                    <input type="radio" name="optradio" checked />Yes
                                  </label>
                                  <label className="radio-inline">
                                    <input type="radio" name="optradio" />No
                                  </label>
                          </div>
                          <div className="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                              <label className="labelHeading">Box length(in km)</label><br/>
                              <input className="createCitiesInput" type="number" placeholder="Box length" />
                          </div>
                          <div className="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <button type="button" className="btn generateButton">Generate</button>
                          </div>
                        </div>
                      <div className="row mapAlignment">
                          <div className="col-12 col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div className="mapouter"><div className="gmap_canvas">
                            <iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=athmin%20nodia&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
                            {/*<style>.mapouter{position:relative;text-align:right;height:300px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:100%;}</style>*/}</div>
                          </div>
                      </div>
                      <div className="row saveButtonRowAlignment">
                          <div className="col-10 col-lg-10 col-md-10 col-sm-10 col-xs-10">
                              <label className="labelHeading">Polygon</label>
                          </div>
                          <div className="col-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <button type="button" className="btn saveButton">Save</button>
                          </div>
                      </div>
                  </div>
              </section>
         </React.Fragment>
        )
    }
}

export default MapRoaster
