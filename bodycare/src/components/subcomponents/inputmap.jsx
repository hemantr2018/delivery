import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow,Polyline } from 'react-google-maps';
import { compose, withProps } from "recompose";

import { get_roaster_routes } from "./../../js/apis/getRoasterRoute";

import MarkerClusterer from "react-google-maps/lib/components/addons/MarkerClusterer";
import { connect } from "react-redux";
import { selectRoaster, setCenterLocation} from "./../../js/actions/index";


var color_array = []
class InputMap extends Component{
	constructor(props) {
		super(props);
		this.state = {
			center: {lat: 28.5150109, lng: 77.0849504},
			zoom: 11,
			order_xls_array: this.props.order_xls_array,
			isMarkerShown: true,
		}
	}

	componentWillReceiveProps(nextProps){
		console.log("IN UNSAFE_componentWillReceiveProps of inputmap");
		if(nextProps.order_xls_array !== this.props.order_xls_array) {
			console.log("In props changed of inputmap");
			this.setState({order_xls_array: nextProps.order_xls_array});
		}
	}
    
    async componentDidMount() {
        console.log("IN componentDidMount of inputmap");
        if(this.props.selected_roaster_id == null) {
            return true;
        }
    }


	render() {
		//const markersArray = [{lat: 28.5150109, lng: 77.0849504},{lat: 28.535517, lng: 77.391029}, {lat: 28.735517, lng: 77.681029}];
		return (
		<div style={{ height: '50vh', margin: 'auto', display: 'block'}}>
			<div style={{ height: '50vh'}}>
				<GoogleMap
					defaultZoom={this.state.zoom}
					defaultCenter={{ lat: this.state.center.lat, lng: this.state.center.lng }}
					//center={this.state.center}
				>
				{console.log("this.state.order_xls_array of inputmap render", this.state.order_xls_array)}
				
				<MarkerClusterer>
				{ this.state.isMarkerShown &&
					this.state.order_xls_array.map((marker_obj, index) => (
						<Marker position={{lat: parseFloat(marker_obj.lat), lng: parseFloat(marker_obj.lon)}} icon={{"url":"img/images.png"}} label={{"text": String(marker_obj.item_count)}}/>
					))
				}
				</MarkerClusterer>
				
				</GoogleMap>
				<div style={{"textAlign": "center"}}><b>Total Orders</b> : {this.state.order_xls_array.length}</div>
			</div>
		</div>
		);
	}
}


function mapStateToProps(state) {
  return {
	selected_roaster_id: state.selected_roaster_id,
	center_location_map: state.center_location_map
  };
}

function mapDispatchToProps(dispatch) {
  return {
    selectRoaster: route_id => dispatch(selectRoaster(route_id)),
    setCenterLocation: center_location => dispatch(setCenterLocation(center_location))
  };
}

//export default (withScriptjs(withGoogleMap(InputMap)))
export default connect(mapStateToProps, mapDispatchToProps)(withScriptjs(withGoogleMap(InputMap)))
