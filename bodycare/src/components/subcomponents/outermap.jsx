import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow,Polyline } from 'react-google-maps';
import { compose, withProps } from "recompose";

import { get_roaster_routes } from "./../../js/apis/getRoasterRoute";

import MarkerClusterer from "react-google-maps/lib/components/addons/MarkerClusterer";
import { connect } from "react-redux";
import { selectRoaster, setCenterLocation} from "./../../js/actions/index";


var color_array = []
class OuterMap extends Component{
	constructor(props) {
		super(props);
		this.state = {
			//center: {lat: 28.5150109, lng: 77.0849504},
			zoom: 11,
			path_objs_array: [],
			isMarkerShown: true,
			route_color_desc: [],
			routes_info: {},
			agents_info: {}
		}
		this.getRandomColor = this.getRandomColor.bind(this);
	}

    getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}

	UNSAFE_componentWillReceiveProps(nextProps){
		console.log("IN componentWillReceiveProps of outermap", nextProps.selectedAgentsList, this.props.selectedAgentsList);
		if(nextProps.selectedAgentsList!==this.props.selectedAgentsList) {
			let all_path_objs_array = this.state.all_path_objs_array;
			let filtered_array  = []
			let agents_ids      = []
			if(nextProps.selectedAgentsList != null) {
				for(var i = 0; i < nextProps.selectedAgentsList.length; i++) {
					agents_ids.push(nextProps.selectedAgentsList[i].value)
				}
			}
			for(var i = 0; i < all_path_objs_array.length; i++) {
				let agent_id = all_path_objs_array[i].agent_id;
				console.log("IN if of componentWillReceiveProps of outermap", agents_ids, agent_id);
				if ( agents_ids.length && agents_ids.indexOf(agent_id) == -1 ) {
					console.log("not considered for", agents_ids, agent_id);
					continue;
				} else {
					console.log("considered for", agents_ids, agent_id);
					filtered_array.push(all_path_objs_array[i])
				}
			}
			this.setState({path_objs_array: filtered_array });
		}
	}
    
    async componentDidMount() {
        console.log("IN componentDidMount of outermap");
        if(this.props.selected_roaster_id == null) {
            return true;
        }
        let colors_array = [];
        let route_data = await get_roaster_routes(this.props.selected_roaster_id);
        let path_objs_array = []
        let path_objs = {}
        let route_color_map = [];
        var route_details = route_data.route_details;
        var routes = route_data.routes;
        var agents = route_data.agents;
        var warehouse_location = route_data.roster.warehouse_loc.split(",");
        var route_agents = {};
        var agent_details = {};
        var agents_ids = []
        
        if(this.props.selectedAgentsList != null) {        
            for(var i = 0; i < this.props.selectedAgentsList.length; i++) {
                agents_ids.push(this.props.selectedAgentsList[i].value)
            }
        }

        for(var i = 0; i < routes.length; i++) {
            route_agents[routes[i]["id"]] = {}
            route_agents[routes[i]["id"]] = {"agent_id": routes[i]["agent_id"], "route_id": routes[i]["id"] }
        }

        for(var i = 0; i < agents.length; i++) {
            agent_details[agents[i]["id"]] = {}
            agent_details[agents[i]["id"]] = {"agent_id": agents[i]["id"], "name": agents[i]["name"], "area": agents[i]["area"]}
        }

        for(var i = 0; i < route_details.length; i++) {
            let agent_id = route_agents[route_details[i].route_id].agent_id;

            if(path_objs.hasOwnProperty(route_details[i].route_id)) {
                path_objs[route_details[i].route_id]["route_id"] =route_details[i].route_id
                path_objs[route_details[i].route_id]["coordinates"].push({"lat": route_details[i]["lat"], "lng": route_details[i]["lon"]})
                path_objs[route_details[i].route_id]["markers_array"].push({"lat": route_details[i]["lat"], "lng": route_details[i]["lon"], "delivery_number": String(route_details[i]["delivery_number"])})
            } else {
                path_objs[route_details[i].route_id] = {}
                path_objs[route_details[i].route_id]["coordinates"] = [{"lat": route_details[i]["lat"], "lng": route_details[i]["lon"]}]
                let random_color = this.getRandomColor();
                while(colors_array.includes(random_color)) {
                    random_color = this.getRandomColor();
                }
                path_objs[route_details[i].route_id]["agent_id"] = agent_id
                path_objs[route_details[i].route_id]["strokeColor"] = random_color;
                colors_array.push(random_color)
                route_color_map.push({"route_id": route_details[i]["route_id"], "color": path_objs[route_details[i].route_id]["strokeColor"]})
                path_objs[route_details[i].route_id]["route_id"] = route_details[i]["route_id"]
                path_objs[route_details[i].route_id]["markers_array"] = []
                path_objs[route_details[i].route_id]["markers_array"].push({"lat": route_details[i]["lat"], "lng": route_details[i]["lon"], "delivery_number": String(route_details[i]["delivery_number"])})
            }
        }
        for (var key in path_objs) {
            if (path_objs.hasOwnProperty(key)) {
                path_objs_array.push(path_objs[key]);
            }
        }
        console.log("path_objs_array", path_objs_array)

        this.setState({
            path_objs_array: path_objs_array,
            all_path_objs_array: path_objs_array,
            route_color_desc: route_color_map,
            routes_info: route_agents,
            agents_info: agent_details,
        });
    }
    
	render() {
		//const markersArray = [{lat: 28.535517, lng: 77.391029}, {lat: 28.735517, lng: 77.681029}]
		const markersArray = [];
		return (
		<div style={{ height: '50vh', margin: 'auto', display: 'block'}}>
		<div style={{ height: '50vh'}}>
		<GoogleMap
			defaultZoom={this.state.zoom}
			defaultCenter={this.props.center_location_map}
			//center={this.state.center}
		>
		<MarkerClusterer>
		{ this.state.isMarkerShown &&
			markersArray.map((markerobj, index) => (
				<Marker position={{lat: markerobj.lat, lng: markerobj.lng}} />
			))
		}
		</MarkerClusterer>
		{/*pathCoordinates.map((pathobj, index) => (*/}
		{ this.state.path_objs_array.map((pathobj, index) => (
		<React.Fragment>
			<Polyline
			   //path={[{ lat: 28.575963, lng: 77.371693 }, { lat: 28.525252, lng: 77.376656 }, { lat: 28.513925, lng: 77.482789 }]}28.5150109,77.0849504
			   path={pathobj.coordinates}
			   options={{
					strokeColor: pathobj.strokeColor,
					strokeOpacity: 1.00,
					strokeWeight: 6
				}}
			/>
			{/*<MarkerClusterer>*/}
				{
				this.props.isDeliveryMarkerShown && pathobj.markers_array.map((marker_obj, index, arr) => (
					<Marker position={{lat: marker_obj.lat, lng: marker_obj.lng}} icon={(arr.length -1 == index) ? {"url":"img/icons8-house-24.png"} : {"url":"img/images.png"} } label={( (arr.length-1) ==index || index==0) ? {}: ({"text": marker_obj.delivery_number})}/>
				))
				}
			{/*</MarkerClusterer>*/}
		</React.Fragment>
			))
		
		}
		</GoogleMap>
	        <div>
				{
					this.state.route_color_desc.map((route_color_obj, index) => (
						<div style={{display:"inline-block"}}>
						<span style={{backgroundColor:route_color_obj.color, display:"inline-block", height: "11px", width: "22px", margin: "0px 5px 0px 1px"}}></span>
						<span>{this.state.agents_info[(this.state.routes_info[route_color_obj.route_id].agent_id)].name}  </span>
						</div>
					))
				}
			</div>
		</div>
		</div>
		);
	}
}

function mapStateToProps(state) {
  return {
	selected_roaster_id: state.selected_roaster_id,
	center_location_map: state.center_location_map
  };
}

function mapDispatchToProps(dispatch) {
  return {
    selectRoaster: route_id => dispatch(selectRoaster(route_id)),
    setCenterLocation: center_location => dispatch(setCenterLocation(center_location))
  };
}

//export default (withScriptjs(withGoogleMap(OuterMap)))
export default connect(mapStateToProps, mapDispatchToProps)(withScriptjs(withGoogleMap(OuterMap)))
