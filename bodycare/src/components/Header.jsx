import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import History from "history";
import { withRouter } from 'react-router-dom';

class Header extends Component {
    constructor(props) {
        super(props);
    }
    
    userLogout = function() {
        console.log("In userLogout");
        localStorage.removeItem('token');
        let path = "/login";
		this.props.history.push(path);
    }
    
    componentDidMount() {
    }

    render() {
		let action_token = localStorage.getItem('token');
        return (
            <nav className="navbar header">
                <div className="container-fluid">
                  <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span className="sr-only">Toggle navigation</span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                      <span className="icon-bar"></span>
                    </button>
                    <a className="navbar-brand" href="#">
                        <img src="img/logo.png" alt="logo-img" style={{"width":"100%"}}/>
                    </a>
                  </div>
                  
                  <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  {action_token != null &&
                    <ul className="nav navbar-nav navbar-right">
                        <li className="userimg">
                            <img src="img/149071.png" alt="user-img" style={{"width":"20%","float":"right"}}/>
                        </li>
                            <li className="dropdown">
                        <a href="#" className="dropdown-toggle userdropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Himanshu<span className="caret"></span></a>
                        <ul className="dropdown-menu">
							<Link to="/"><li><a href="#">View Roasters</a></li></Link>
							<Link to="/cities"><li><a href="#">View Cities</a></li></Link>
							<Link to="#"><li onClick={this.userLogout.bind(this)}><a href="#">Logout</a></li></Link>
                          {/*<Link to="/map-roaster"><li><a href="#">Add City</a></li></Link>*/}
                          {/*<Link to="/create-roaster"><li><a href="#">New Roaster</a></li></Link>*/}
                          {/*<Link to="/roaster-details"><li><a href="#">Roaster Details</a></li></Link>
                          <li><a href="#">Another action</a></li>
                          <li><a href="#">Something else here</a></li>
                          <li role="separator" className="divider"></li>
                          <li><a href="#">Separated link</a></li>*/}
                        </ul>
                      </li>
                    </ul>
                  }
                  </div>
                </div>
              </nav>
        )
    }
}

//export default Header
export default withRouter(Header);
