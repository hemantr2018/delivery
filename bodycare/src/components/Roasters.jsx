import React, { Component } from 'react';
import './../css/roster.css';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import { get_all_roaster } from "./../js/apis/getAllRoasters";
import { selectRoaster } from "./../js/actions/index";


let status_array = ["", "Created", "Agents Uploaded", "Orders Uploaded", "Processing", "Processed", "Routing", "Routed", "Failed", "Inactive"]
class Roasters extends Component {
    constructor(props) {
        super(props);
        this.state = {
			roaster_details_list : []
        } 
    }

	
	viewRoute = function (roaster_id) {
		console.log("IN viewRoute");
		console.log(roaster_id);
		this.props.selectRoaster(roaster_id);
		let path = "/create-roaster";
		this.props.history.push(path);
		
	}
	
	create_new = function () {
		this.props.selectRoaster(null);
		let path = "/create-roaster";
		this.props.history.push(path);
		//this.props.router.push('/some/path')
	}

    async componentDidMount() {
		console.log("In if of raosters");
		let action_token = localStorage.getItem('token');
        if (action_token == null) {
			let path = "/login";
			this.props.history.push(path);
        } else {
			console.log("In else");
			let roaster_details = await get_all_roaster();
			console.log(roaster_details);
			this.setState({"roaster_details_list": roaster_details})
        }
    }

    render() {
        //console.log("selected_roaster_id", this.props.selected_roaster_id)
        return (
           <React.Fragment>
           <section className="headingRow">
                <div className="container">
                  <div className="row">
                    <div className="col-md-1 col-xs-1 col-sm-1 col-1 col-lg-1">
                      <p className="rosterHeading">Roster</p>
                      <p className="rosterHeadingLine"></p>
                    </div>
                    <div className="col-md-9 col-xs-9 col-sm-9 col-9 col-lg-9"></div>
                    <div className="col-md-2 col-xs-2 col-sm-2 col-2 col-lg-2">
                        {/*<Link to="/create-roaster">*/}
                        <button type="button" onClick={this.create_new.bind(this)} className="btn createRosterBtn btn-sm">Create New  <i className="fa fa-plus"></i></button>
                        {/*</Link>*/}
                      </div>
                  </div>
                </div>
              </section>
              <section>
                <div className="container">
                  <div className="table-responsive">
                <table className="table table-striped">
                  <thead>
                      <tr>
                          {/*<th>Created Time</th>
                          <th>Label</th>
                          <th>City</th>
                          <th>Schedule Date</th>
                          <th>Discard</th>
                          <th>User</th>*/}
                          <th>Name</th>
                          <th>Slot Id</th>
                          <th>Warehouse Grid</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                  </thead>
                  <tbody>
                    {/*<td>01-Apr-2019</td>
                    <td>Test</td>
                    <td>Delhi</td>
                    <td>12-Apr-2019</td>
                    <td>No</td>
                    <td>Gaurav</td>*/}
                    {this.state.roaster_details_list.map((roasterobj, index) => (
						<tr>
							<td>{roasterobj.name !== "" ? roasterobj.name : 'N/A'}</td>
							<td>{roasterobj.slot_id}</td>
							<td>{roasterobj.warehouse_grid}</td>
							<td>{status_array[roasterobj.status]}</td>
							<td className="editButton">{/*<i className="fa fa-pencil"></i>Edit */} <a><span onClick={this.viewRoute.bind(this, roasterobj.id)}>ViewRoaster</span></a></td>
						</tr>
					))}
                  
                  {/*<tr>
                    <td>02-Apr-2019</td>
                    <td>Test</td>
                    <td>Delhi</td>
                    <td>14-Apr-2019</td>
                    <td>No</td>
                    <td>Gaurav</td>
                    <td className="editButton"><i className="fa fa-pencil"></i>Edit</td>
                  </tr>*/}
                </tbody>
                </table>
              </div>
              </div>
              </section>
         </React.Fragment>
        )
    }
}

function mapStateToProps(state){
  return { selected_roaster_id: state.selected_roaster_id };
}

function mapDispatchToProps(dispatch) {
  return {
    selectRoaster: route_id => dispatch(selectRoaster(route_id))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Roasters)
