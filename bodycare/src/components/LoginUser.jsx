import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './../css/login.css';
import { connect } from "react-redux";

import { user_login } from "./../js/apis/login";
import FlashMassage from 'react-flash-message';


class LoginUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
			"message": ""
        }
    }

    userLogin = async function() {
        console.log("In userLogin");
        let param_json = {}
        let user_name = document.getElementById("uname").value;
		let password = document.getElementById("psw").value;
		if(user_name != "" && password != "") {
			param_json.user_name = user_name;
			param_json.password = password;
			let user_token = await user_login(param_json);
			if(user_token) {
				let path = "/";
				this.props.history.push(path);
			} else {
				this.setState({
				"message": "Username/Password is incorrect"
				});
			}
		} else {
			this.setState({
				"message": "Please enter all mandatory fields"
			});
		}
    }

    async componentDidMount() {
        console.log("In componentDidMount of user login");
    }
    

    render() {
        return (
           <React.Fragment>
           <section className="headingRow">
                <div className="container">
                  <div className="row">
                    <div className="col-md-1 col-xs-1 col-sm-1 col-1 col-lg-1">
                      <p className="citiesHeading">Login</p>
                      <p className="citiesHeadingLine"></p>
                    </div>
                    <div className="col-md-6 col-xs-6 col-sm-6 col-6 col-lg-6"></div>
                    <div className="col-md-3 col-xs-3 col-sm-3 col-3 col-lg-3">
                    <div className="search"></div>
                    </div>
                    <div className="col-md-2 col-xs-2 col-sm-2 col-2 col-lg-2"></div>
                  </div>
                </div>
              </section>
              <section>
                <div class="container">
					<div class="row">
						{this.state.message != "" &&
							<div style={{"textAlign": "center", "fontWeight": "700", "fontSize": "14px", "color": "red"}}><FlashMassage duration={20000} persistOnHover={true}><p>{this.state.message}</p></FlashMassage></div>
						}
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-1"></div>
								<label for="uname" class="col-md-3"><b>Username</b></label>
								<input class="col-md-7" type="text" placeholder="Enter Username" id="uname" name="uname" required/>
								<div class="col-md-1"></div>
							</div>
							<div class="row">
								<div class="col-md-1"></div>
								<label class="col-md-3" for="psw"><b>Password</b></label>
								<input class="col-md-7" type="password" placeholder="Enter Password" id="psw" name="psw" required/>
								<div class="col-md-1"></div>
							</div>
							<div class="row">
								<div class="col-md-9"></div>
								<div class="col-md-3">
									<button type="submit" onClick={this.userLogin.bind(this)}>Login</button>
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					{/*<div class="row">
						<div class="col-md-2"></div>
						<label class="col-md-4" for="psw"><b>Password</b></label>
						<input class="col-md-4" type="password" placeholder="Enter Password" name="psw" required/>
						<div class="col-md-2"></div>
					</div>
					<div class="col-md-5"></div>
					<div class="col-md-2"><button type="submit">Login</button></div>
					<div class="col-md-5"></div>*/}
				</div>
              </section>
         </React.Fragment>
        )
    }
}

function mapStateToProps(state){
  return { articles: state.articles };
}

export default connect(mapStateToProps)(LoginUser)
