import React, { Component } from 'react';
import {inputvisualization, output, input, outputvisualization} from './../js/createroster.js';
import OuterMap from './subcomponents/outermap';
import InputMap from './subcomponents/inputmap';
import './../css/createroster.css';

import { get_roaster_agents } from "./../js/apis/getRoasterAgents";
import { get_roaster_routes } from "./../js/apis/getRoasterRoute";
import { get_roaster_orders } from "./../js/apis/getRoasterOrders";
import { connect } from "react-redux";
import { selectRoaster, setCenterLocation} from "./../js/actions/index";
import Select from 'react-select';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';

import { fetch_city } from "./../js/apis/citieslist";
import { fetch_algos } from "./../js/apis/algoslist";
import { create_roaster } from "./../js/apis/createRoaster";
import { create_roaster_agents } from "./../js/apis/createRoasterAgents";
import { create_roaster_orders } from "./../js/apis/createRoasterOrders";
import { process_roasters } from "./../js/apis/processRoasters";
import { generate_roaster_route } from "./../js/apis/generateRoasterRoute";
import { get_roaster_routes_excel } from "./../js/apis/getRoasterRouteExcel";
import FlashMassage from 'react-flash-message';
import { Link } from 'react-router-dom';
let CONFIG = require('./../lib/config.json')

let agent_xls_array = [];
let order_xls_array = [];
let base_url = CONFIG.server_api_url;
class CreateRoaster extends Component {
    constructor(props) {
        super(props);
        this.state = {
			isDeliveryMarkerShown: false,
			selectedOption: null,
			selectedCityOption: null,
			selectedAlgoOption: null,
			agents_list : [],
			city_list : [],
			algo_list : [],
			output_obj_array: [],
			roaster_orders: [],
			status: null,
			message: "",
			agent_xls_array: [],
			order_xls_array: []
        }
    }
	handleChange = selectedOption => {
		this.setState(
		  { selectedOption },
		  () => console.log(`Option selected:`, this.state.selectedOption)
		);
	};
	
	
	viewRoute = function(agent_id, agent_name) {
		outputvisualization();
		this.setState({ selectedOption: [{"label": agent_name, "value": agent_id}] });
	}
	
	handleCityChange = selectedCityOption => {
		this.setState(
		  { selectedCityOption },
		  () => console.log(`Option selected:`, this.state.selectedCityOption)
		);
	}

	handleAlgoChange = selectedAlgoOption => {
		this.setState(
		  { selectedAlgoOption },
		  () => console.log(`Option algo selected:`, this.state.selectedAlgoOption)
		);
	}

	result(params) {
		console.log(params);
	}
	
	async getRoasterRoutesExcel() {
		let resp_process = await get_roaster_routes_excel(this.props.selected_roaster_id);
	}

	handleDeliveryMarkerCheck = function() {
		this.setState({isDeliveryMarkerShown: !this.state.isDeliveryMarkerShown});
	}

	agentFileHandler = (event) => {
		let fileObj = event.target.files[0];

		//just pass the fileObj as parameter
		ExcelRenderer(fileObj, (err, resp) => {
			if(err){
				console.log(err);
			}
			else {
				agent_xls_array = [];
				let column_length = resp.cols.length;
				for(var i = 1; i < resp.rows.length; i++) {
					let agent_obj = {}
					if((resp.rows[i]).length == 0) {
						break;
					}
					for(var j = 0; j < column_length; j++) {
						agent_obj[(resp.rows[0][j]).trim()] = (String(resp.rows[i][j])).trim()
					}
					agent_xls_array.push(agent_obj)
				}
				this.setState({"agent_xls_array": agent_xls_array});
			}
		});
	}
	
	orderFileHandler = (event) => {
		let fileObj = event.target.files[0];
		//just pass the fileObj as parameter
		ExcelRenderer(fileObj, (err, resp) => {
			if(err){
				console.log(err);
			}
			else {
				order_xls_array = [];
				let column_length = resp.cols.length;
				console.log("resp.rows", resp.rows);
				for(var i = 1; i < resp.rows.length; i++) {
					if((resp.rows[i]).length == 0) {
						break;
					}
					let order_obj = {}
					for(var j = 0; j < column_length; j++) {
						order_obj[(resp.rows[0][j]).trim()] = (String(resp.rows[i][j])).trim()
					}
					order_xls_array.push(order_obj)
				}
				console.log("order_xls_array", order_xls_array);
				this.setState({"order_xls_array": order_xls_array});
			}
		});
	}
	
	async generateRoasterRoute() {
		let resp_generate = await generate_roaster_route(this.props.selected_roaster_id, {"algo_id": this.state.selectedAlgoOption.value});
		if(resp_generate) {
			this.setState({"status": 6, "message": "Routes generation initiated, it will take around 5-10 minutes in completing"});
		}
		
	}
	
	async processRoasters() {
		
		let resp_process = await process_roasters(this.props.selected_roaster_id, null);
		if(resp_process) {
			this.setState({"status": 4, "message": "Processing Roasters initiated, it will take around 5-10 minutes in completing"});
		}
	}
  
	async createRoaster() {
		document.getElementById("loader").style.display = "block";
		document.getElementById("createRoasterButton").disabled = true;
		let param_json = {}
		let agent_file = document.getElementById("agentFile").value;
		let order_file = document.getElementById("orderFile").value;
		let warehouse_lat= document.getElementById("warehouse_lat").value;
		let warehouse_long= document.getElementById("warehouse_long").value;
		let schedule_date= document.getElementById("schedule_date").value;
		let delivery_time= document.getElementById("delivery_time").value;
		let delivery_city= this.state.selectedCityOption;
		let delivery_label= document.getElementById("delivery_label").value;
		let roaster_status= document.getElementById("roaster_status").checked;
		if(agent_file != "" && order_file != "" && warehouse_lat != "" &&  warehouse_long != "" && schedule_date !="" && delivery_time !="" && delivery_city != null && delivery_label !="") {
			param_json.warehouse_lat = warehouse_lat // 28.5150109
			param_json.warehouse_lon = warehouse_long // 77.0849504
			param_json.name = delivery_label
			param_json.slot_id = schedule_date.replace(/-/g,"")
			param_json.city_id = delivery_city.value
			param_json.delivery_time = delivery_time
			param_json.roaster_status = roaster_status
			let resp = await create_roaster(param_json);
			if (Object.keys(resp).length !== 0 ) {
				let roaster_id = resp.roster_id;
				let resp_agents = await create_roaster_agents(roaster_id, agent_xls_array);
				if(resp_agents != "success") {
					console.log("Error while adding agents");
				}
				let resp_orders = await create_roaster_orders(roaster_id, order_xls_array);
				if(resp_orders != "success") {
					console.log("Error while adding orders");
				}
				/*NO ERROR*/
				this.props.selectRoaster(roaster_id);
				document.getElementById("loader").style.display = "none";
				this.setState({"roaster_orders": order_xls_array, "status": 3, "message": "Roaster Created Successfully"});
			} else {
				document.getElementById("loader").style.display = "none";
				document.getElementById("createRoasterButton").disabled = false;
				this.setState({"message": "Error while creating roaster"});
				// Error while creating roaster, Show ERROR MESSAGE
			}
		} else {
			// Show ERROR MESSAGE
			document.getElementById("createRoasterButton").disabled = false;
			document.getElementById("loader").style.display = "none";
			this.setState({"message": "Enter all mandatory fields including excels"});
		}
	}

	async componentDidMount() {
		let city_info = await fetch_city();
        let city_lists = [];
        let algo_lists = [];
        for(var i = 0; i < city_info.length; i++) {
			city_lists.push( {"label": city_info[i].name, "value": city_info[i].id} )
        }
        let algo_info = await fetch_algos();
        for(var i = 0; i < algo_info.length; i++) {
			algo_lists.push( {"label": algo_info[i].name, "value": algo_info[i].id} )
        }
		let agents_list = [];
		if(this.props.selected_roaster_id == null) {
			this.setState({
				city_list: city_lists,
				algo_list: algo_lists
			});
			return true;
        }
       
		let agents_info = await get_roaster_agents(this.props.selected_roaster_id);
        for(var i = 0; i < agents_info.length; i++) {
			agents_list.push({"label": agents_info[i].name, "value": agents_info[i].id})
        }
        
        let roaster_orders = await get_roaster_orders(this.props.selected_roaster_id);
        let order_xls_array = []
        for(var i = 0; i < roaster_orders.length; i++) {
			order_xls_array.push({"lat": roaster_orders[i].point.split(",")[0], "lon": roaster_orders[i].point.split(",")[1], "item_count": roaster_orders[i].item_count})
        }

        let route_data = await get_roaster_routes(this.props.selected_roaster_id);
        var warehouse_location = route_data.roster.warehouse_loc.split(",");
        let roaster_name = route_data.roster.name;
        let status = route_data.roster.status;

        if(warehouse_location && document.getElementById("warehouse_lat")!=null) {
			document.getElementById("warehouse_lat").value=warehouse_location[0].trim();
			document.getElementById("warehouse_long").value=warehouse_location[1].trim();
			document.getElementById("delivery_label").value=roaster_name;
        }
        
        
        var route_details = route_data.route_details;
        var routes = route_data.routes;
        var agents = route_data.agents;
        var route_agents = {};
        var agent_details = {};
        var output_obj = {}
        let output_obj_array = [];
        for(var i = 0; i < routes.length; i++) {
            route_agents[routes[i]["id"]] = {}
            route_agents[routes[i]["id"]] = {"agent_id": routes[i]["agent_id"], "route_id": routes[i]["id"], "total_time": routes[i]["time"], "total_distance": routes[i]["distance"]}
        }
        
        for(var i = 0; i < agents.length; i++) {
            agent_details[agents[i]["id"]] = {}
            agent_details[agents[i]["id"]] = {"agent_id": agents[i]["id"], "name": agents[i]["name"], "area": agents[i]["area"]}
        }
        
        for(var i = 0; i < route_details.length; i++) {
            let agent_id = route_agents[route_details[i].route_id].agent_id;
            var order_ids = []
            for(var j = 0; j < route_details[i]["orders"].length; j++) {
				order_ids.push(route_details[i]["orders"][j]["order_id"]);
            }
            if(output_obj.hasOwnProperty(agent_id)) {
                output_obj[agent_id]["order_details"].push({"order_number": order_ids.join(", "), "sequence_number": route_details[i].delivery_number});
            } else {
                let agent_name = agent_details[agent_id].name;
                output_obj[agent_id] = {"agent_id":agent_id, "agent_name": agent_name, "order_details": [], "total_time": route_agents[route_details[i].route_id].total_time, "total_distance": route_agents[route_details[i].route_id].total_distance}
                output_obj[agent_id]["order_details"].push({"order_number": order_ids.join(", "), "sequence_number": route_details[i].delivery_number});
            }
        }

        for (var key in output_obj) {
            if (output_obj.hasOwnProperty(key)) {
                output_obj_array.push(output_obj[key]);
            }
        }
        this.props.setCenterLocation({"lat": parseFloat(warehouse_location[0]), "lng": parseFloat(warehouse_location[1])});
        let message = ""
        if(status==4){
			message = "Processing agents currently, please wait for sometime"
        } else if(status==6) {
			message = "Processing routes currently, please wait for sometime"
        }
        
        this.setState({
			agents_list: agents_list,
			city_list: city_lists,
			algo_list: algo_lists,
			output_obj_array: output_obj_array,
			route_data: route_data,
			roaster_orders: order_xls_array,
			selectedCityOption: city_lists[0],
			status: status,
			message: message
        });

	}

    render() {
        return (
           <React.Fragment>
           <section>
                <div className="container">
                  <div className="row headingRow">
                    <div className="col-md-3 col-xs-3 col-sm-3 col-3 col-lg-3">
                      <p className="createRosterHeading">{this.props.selected_roaster_id !=null ? "View Roster": "Create Roster"}</p>
                      <p className="createRosterHeadingLine"></p>
                    </div>
                    <div className="col-md-7 col-xs-7 col-sm-7 col-7 col-lg-7"></div>
                    <div className="col-md-2 col-xs-2 col-sm-2 col-2 col-lg-2">
                        <Link to="/"><button type="button" className="btn addCitiesBtn btn-sm">Back</button></Link>
                    </div>
                  </div>
                  <div className="row">
                      <div className="col-2 col-md-2 col-lg-2 col-sm-2 col-xs-2"></div>
                      <div className="col-2 col-md-2 col-lg-2 col-sm-2 col-xs-2">
                      <button type="button" className="inputbutton tabHeading tabActive" onClick={input}>Input</button>
                      </div>
                      <div className="col-2 col-md-2 col-lg-2 col-sm-2 col-xs-2">
							{console.log("this.props.selected_roaster_id",this.props.selected_roaster_id)}
                            <button type="button" disabled={this.props.selected_roaster_id !=null ? false: true} className="inputvisualizationbutton tabHeading" onClick={inputvisualization}>Input Visualization</button>
                      </div>
                      <div className="col-2 col-md-2 col-lg-2 col-sm-2 col-xs-2">
                            <button type="button" disabled={this.props.selected_roaster_id !=null ? false: true} className="outputbutton tabHeading" onClick={output}>Output</button>
                      </div>
                      <div className="col-2 col-md-2 col-lg-2 col-sm-2 col-xs-2">
                            <button type="button" disabled={this.props.selected_roaster_id !=null ? false: true} className="outputvisualizationbutton tabHeading" onClick={outputvisualization}>Output Visualization</button>
                      </div>
                      <div className="col-2 col-md-2 col-lg-2 col-sm-2 col-xs-2"></div>
                  </div>
                  <div class="loader" id="loader" style={{"display": "none"}}></div>
                </div>
              </section>
              <section className="outputVisualizationCard input">
                <div className="container">
                  <div className="row">
					{this.state.message != "" &&
						<div style={{"textAlign": "center", "fontWeight": "700", "fontSize": "14px", "color": "red"}}><FlashMassage duration={20000} persistOnHover={true}><p>{this.state.message}</p></FlashMassage></div>
					}
                    <div className="col-6 col-md-6 col-lg-6 col-sm-6 col-xs-6 inputFormAlignment">
                        <div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12 inputFormHeadingAlignment">
                          <span className="inputFormHeading">Delivery Agent File</span>
                          <input disabled={this.props.selected_roaster_id ==null ? false: true} type="file" id="agentFile" className="" style={{"width": "40%", "display": "inline", "margin": "0 5% 0 5%"}} onChange={this.agentFileHandler.bind(this)} />
                        </div>
                      <div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12">
                        <p className="inputFormLabel">Warehouse Location Lat</p>
                        <input disabled={this.props.selected_roaster_id ==null ? false: true} className="inputFormField" type="number" id="warehouse_lat" placeholder="lat" step="0.01"/>
                      </div>
                      <div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12">
                        <p className="inputFormLabel">Warehouse Location Long</p>
                        <input disabled={this.props.selected_roaster_id ==null ? false: true} className="inputFormField" type="number" id="warehouse_long" placeholder="long" step="0.01"/>
                      </div>
                      <div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12">
                        <p className="inputFormLabel">Schedule Date</p>
                        <input disabled={this.props.selected_roaster_id ==null ? false: true} className="inputFormField" id="schedule_date" type="date" placeholder="--/--/----"/>
                      </div>
                    </div>
                    <div className="col-6 col-md-6 col-lg-6 col-sm-6 col-xs-6 inputFormAlignment">
                        <div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12 inputFormHeadingAlignment">
                            <span className="inputFormHeading">Order File</span>
                            <input disabled={this.props.selected_roaster_id ==null ? false: true} type="file" id="orderFile" className="" style={{"width": "40%", "display": "inline", "margin": "0 5% 0 5%"}} onChange={this.orderFileHandler.bind(this)} />
                        </div>
                      <div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12">
                        <p className="inputFormLabel">Delivery Time/Address</p>
                        <input disabled={this.props.selected_roaster_id ==null ? false: true} className="inputFormField" id="delivery_time" type="text" placeholder="Delivery Time/Address"/>
                      </div>
                      <div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12">
                        <p className="inputFormLabel">City</p>
                        <Select className="inputFormField noBorderBottom"
							value={this.state.selectedCityOption}
							onChange={this.handleCityChange}
							options={this.state.city_list}
						/>
                      </div>
                      <div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12">
                        <p className="inputFormLabel">Label</p>
                        <input disabled={this.props.selected_roaster_id ==null ? false: true} className="inputFormField" type="text" id="delivery_label" placeholder="Label"/>
                      </div>
                    </div>
                  </div>
                  <div className="row inputFormSubmitRow">
					<div className="col-md-7 col-lg-7 col-7 col-sm-7 col-xs-7">
					{this.props.selected_roaster_id == null && 
						<div className="checkbox">
							<label style={{"color":"black","font-weight":"600"}}><input disabled={this.props.selected_roaster_id ==null ? false: true} type="checkbox" id="roaster_status" value=""/>Discard</label>
						  </div>
					}
					</div>
                    {this.props.selected_roaster_id != null && this.state.status == 3 &&
						<React.Fragment>
						<div className="col-md-2 col-lg-2 col-2 col-sm-2 col-xs-2"></div>
						<div className="col-md-2 col-lg-2 col-2 col-sm-2 col-xs-2">
							<button type="button" className="inputFormButton" onClick={this.processRoasters.bind(this)}>Process</button>
						</div>
						</React.Fragment>
                    }
                    {console.log("this.state.status",this.state.status)}
                    {this.props.selected_roaster_id != null && this.state.status == 5 &&
						<React.Fragment>
						<div className="col-md-5 col-lg-5 col-5 col-sm-5 col-xs-5">
							<div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12">
								<p className="inputFormLabel">Route Algo</p>
								<Select className="inputFormField noBorderBottom"
									value={this.state.selectedAlgoOption}
									onChange={this.handleAlgoChange}
									options={this.state.algo_list}
								/>
							</div>
							<div className="col-md-12 col-lg-12 col-12 col-sm-12 col-xs-12">
								<button type="button" className="inputFormButton" onClick={this.generateRoasterRoute.bind(this)}>Create Route</button>
							</div>
						</div>
						</React.Fragment>
                    }
                    {this.props.selected_roaster_id == null && this.state.status == null &&
						<React.Fragment>
							<div className="col-md-2 col-lg-2 col-2 col-sm-2 col-xs-2"></div>
							<div className="col-md-2 col-lg-2 col-2 col-sm-2 col-xs-2">
								<button disabled={this.props.selected_roaster_id ==null ? false: true} id="createRoasterButton" type="button" className="inputFormButton" onClick={this.createRoaster.bind(this)}>Save</button>
							</div>
							<div className="col-md-1 col-lg-1 col-1 col-sm-1 col-xs-1"></div>
						</React.Fragment>
					}
					</div>
					<div className="row">
					<div className="table-responsive" className="col-md-5 col-lg-5 col-5 col-sm-5 col-xs-5">
						{this.state.agent_xls_array.length > 0 && 
						<React.Fragment>
						<div style={{"textAlign": "center"}}><b>Agents Info</b></div>
						<div style={{"maxHeight": "25rem", "overflowY":"scroll", "overflowX":"none"}}>
						<table className="table table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Area</th>
									<th>Area Location</th>
									<th>Home Location</th>
								</tr>
							</thead>
							<tbody>
								{this.state.agent_xls_array.map((agent_obj, index) => (
								<tr>
									<td>{agent_obj.name}</td>
									<td>{agent_obj.area}</td>
									<td>{agent_obj.area_lat}, {agent_obj.area_lon}</td>
									<td>{agent_obj.home_lat}, {agent_obj.home_lon}</td>
								</tr>
								))}
							</tbody>
						</table>
						</div>
						</React.Fragment>
						}
						</div>
					<div className="col-md-1 col-lg-1 col-1 col-sm-1 col-xs-1"></div>
						<div className="table-responsive" className="col-md-5 col-lg-5 col-5 col-sm-5 col-xs-5">
							{this.state.order_xls_array.length > 0 && 
							<React.Fragment>
							<div style={{"textAlign": "center"}}><b>Orders Info</b></div>
							<div style={{"maxHeight": "25rem", "overflowY":"scroll", "overflowX":"none"}}>
							<table className="table table-striped">
								<thead>
									<tr>
										<th>Order ID</th>
										<th>Location</th>
										<th>Item Count</th>
									</tr>
								</thead>
								<tbody>
									
									{this.state.order_xls_array.map((order_obj, index) => (
										<tr>
											<td>{order_obj.order_id}</td>
											<td>{order_obj.lat}, {order_obj.lon}</td>
											<td>{order_obj.item_count}</td>
										</tr>
									))}
								</tbody>
							</table>
							</div>
							</React.Fragment>
							}
						</div>
					</div>
                </div>
              </section>
              <section className="outputVisualizationCard inputvisualization">
                <div className="container">
                  <div className="row outputVisualizationMap">
                    <div className="col-12 col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    
                    
                    
						<div className="mapouter">
                            <div className="gmap_canvas">
								{/*<iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=athmin%20nodia&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>*/}
								{/*googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBO--EWGOVt6iLZLxvObtS9QwBWF53NQEM&v=3.exp&libraries=geometry,drawing,places"*/}
								<InputMap
									googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCCpwVmFp_9nMxyWEjIiioNyCo5AdL1ok&v=3.exp&libraries=geometry,drawing,places"
									loadingElement={<div title="map1" id="gmap_canvas" style={{ height: "700px", width: "100%", frameborder: "0", scrolling: "no", marginheight:"0", marginwidth:"0" }} />}
									containerElement={<div title="map2" id="gmap_canvas1" style={{ height: "700px", width: "100%" }} />}
									mapElement={<div title="map3" id="gmap_canvas2" style={{ height: "700px", width: "100%" }} />}
									selected_roaster_id={this.props.selected_roaster_id}
									order_xls_array={this.state.roaster_orders}
								>
								</InputMap>
                            </div>
                            {/*<style>.mapouter{position:relative;text-align:right;height:300px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:100%;}</style>*/}
                            </div>
                    
                    
                    
                            
						
                      {/*<div className="mapouter"><div className="gmap_canvas"><iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=athmin%20nodia&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
                      <style>.mapouter{position:relative;text-align:right;height:300px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:100%;}</style></div>*/}
                      
                      
                        </div>
                      </div>
                </div>
              </section>
              <section style={{"margin-top":"3%"}} className="output">
                    <div className="container">
                      <div className="table-responsive">
                    <table className="table table-striped">
                      <thead>
                          <tr>
                              <th>Delivery Agent Id</th>
                              <th>Name</th>
                              <th>Order Id</th>
                              <th>Sequence No</th>
                              <th>Total Distance(In Km)</th>
                              <th>Total Time(In minutes)</th>
                              <th onClick={this.getRoasterRoutesExcel.bind(this)}><a target="new" href="#">Download Excel<span class="glyphicon glyphicon-download"></span></a></th>
                            </tr>
                      </thead>
                      <tbody>
                      { this.state.output_obj_array.map((output_obj, index) => (
                      <tr>
                        <td>{output_obj.agent_id}</td>
                        <td>{output_obj.agent_name}</td>
                            <td>
								{ output_obj['order_details'].map((order_obj, index) => (
									<span>{order_obj.order_number}<br/></span>
								))}
                            </td>
                            <td>
								{ output_obj['order_details'].map((order_obj, index) => (
									<span>{order_obj.sequence_number}<br/></span>
								))}
                            </td>
                        <td>{(output_obj.total_distance)/1000}</td>
                        <td>{((output_obj.total_time)/60).toFixed(2)}</td>
                        <td className="editButton"><a onClick={this.viewRoute.bind(this, output_obj.agent_id, output_obj.agent_name)}>View Route</a></td>
                      </tr>
                      ))}
                      {/*<tr>
                        <td>12</td>
                        <td>Gaurav</td>
                        <td>13<br/>89<br/>98</td>
                        <td>1<br/>2<br/>3</td>
                        <td>82</td>
                        <td>95</td>
                        <td className="editButton">View Route</td>
                      </tr>*/}
                    </tbody>
                    </table>
                  </div>
                  </div>
                  </section>
                  <section className="outputVisualizationCard outputvisualization">
                      <div className="container">
                          <div className="row">
                              <div className="col-6 col-lg-6 col-sm-6 col-md-6 col-xs-6"></div>
                              <div className="col-2 col-lg-2 col-sm-2 col-md-2 col-xs-2">
								<div style={{"marginTop": "15%"}}><label class="checkbox-inline"><input type="checkbox" onChange={this.handleDeliveryMarkerCheck.bind(this)} defaultChecked={this.state.isDeliveryMarkerShown} value=""/>Show Delivery Points</label></div>
							  </div>
                              <div className="col-3 col-lg-3 col-sm-3 col-md-3 col-xs-3">
                                    <div className="search" style={{"paddingTop": "3%"}}>
                                     <Select
										value={this.state.selectedOption}
										onChange={this.handleChange}
										options={this.state.agents_list}
										isMulti={true}
									  />
										{/*<i className="fa fa-search"></i>
										<input type="text" className="outputVisualizationSearch" placeholder="Search By Agent Id"/>*/}
                                    </div>
                              </div>
                              <div className="col-1 col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                          </div>
                          <div className="row outputVisualizationMap">
                          <div className="col-12 col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div className="mapouter">
                            <div className="gmap_canvas">
								{/*<iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=athmin%20nodia&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>*/}
								<OuterMap
									googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCCpwVmFp_9nMxyWEjIiioNyCo5AdL1ok&v=3.exp&libraries=geometry,drawing,places"
									loadingElement={<div title="map1" id="gmap_canvas" style={{ height: "700px", width: "100%", frameborder: "0", scrolling: "no", marginheight:"0", marginwidth:"0" }} />}
									containerElement={<div title="map2" id="gmap_canvas1" style={{ height: "700px", width: "100%" }} />}
									mapElement={<div title="map3" id="gmap_canvas2" style={{ height: "700px", width: "100%" }} />}
									selected_roaster_id={this.props.selected_roaster_id}
									isDeliveryMarkerShown={this.state.isDeliveryMarkerShown}
									selectedAgentsList={this.state.selectedOption}
									//center_location_map={this.props.center_location_map}
								>
								</OuterMap>
                            </div>
                            {/*<style>.mapouter{position:relative;text-align:right;height:300px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:100%;}</style>*/}
                            </div>
                              </div>
                            </div>
                      </div>
                      
                  </section>
         </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
  return {
	selected_roaster_id: state.selected_roaster_id,
	center_location_map: state.center_location_map
  };
}

function mapDispatchToProps(dispatch) {
  return {
    selectRoaster: route_id => dispatch(selectRoaster(route_id)),
    setCenterLocation: center_location => dispatch(setCenterLocation(center_location))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateRoaster)
