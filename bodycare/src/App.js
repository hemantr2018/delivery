import React from 'react';
//import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import { MemoryRouter as Router, Route, Link, Switch } from 'react-router-dom';
import Header from './components/Header';
import CitiesCmp from './components/CitiesCmp';
import MapRoaster from './components/MapRoaster';
import Roasters from './components/Roasters';
import CreateRoaster from './components/CreateRoaster';
import RoasterDetails from './components/RoasterDetails';
import LoginUser from './components/LoginUser';


//const Home = () => <h1>Home</h1>;
//const Admin = () => <h1>Admin</h1>;


const NavBar = () => (
  <ul>
    <li>
      {/*<Link to="/cities">cities</Link>*/}
      <Link to="/">cities</Link>
    </li>
    <li>
    <Link to="/map-roaster">map-roaster</Link>
    </li>
    <li>
    <Link to="/roasters">roasters</Link>
    </li>
    <li>
    <Link to="/create-roaster">create-roaster</Link>
    </li>
    <li>
    <Link replace={true} to="/roaster-details">roaster-details</Link>
    </li>
  </ul>
);

function App() {
  return (
    <React.Fragment>
        <Router>
			<Header></Header>
        <Switch>
			<Route exact path="/login" component={LoginUser} />
            <Route exact path="/cities" component={CitiesCmp} />
            <Route exact path="/map-roaster" component={MapRoaster} />
            {/*<Route exact path="/roasters" component={Roasters} />*/}
            <Route exact path="/" component={Roasters} />
            <Route exact path="/create-roaster" component={CreateRoaster} />
            <Route exact path="/roaster-details" component={RoasterDetails} />
        </Switch>
        </Router>
        {/*<footer className="App-header">{NavBar}</footer>*/}
  </React.Fragment>
  );
}

export default App;
