import { SELECTED_ROASTER, SET_CENTER_LOCATION } from "../constants/action-types";


const initialState = {
  selected_roaster_id: null,
  center_location_map: {"lat": 28.5150109, "lng": 77.0849504},
  //selected_roaster_id: null,
  //center_location_map: {"lat": 28.575963, "lng": 77.371693}
};

function rootReducer(state = initialState, action) {
  if (action.type === SELECTED_ROASTER) {
    return Object.assign({}, state, {
      //selected_roaster_id: state.selected_roaster_id.concat(action.payload)
		selected_roaster_id: action.payload
    });
  }
  if (action.type === SET_CENTER_LOCATION) {
    return Object.assign({}, state, {
      //selected_roaster_id: state.selected_roaster_id.concat(action.payload)
		center_location_map: action.payload
    });
  }
  return state;
};
export default rootReducer;
