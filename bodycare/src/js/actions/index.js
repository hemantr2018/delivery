import { SELECTED_ROASTER, SET_CENTER_LOCATION } from "../constants/action-types";

export function selectRoaster(payload) {
  return { type: SELECTED_ROASTER, payload }
};

export function setCenterLocation(payload) {
  return { type: SET_CENTER_LOCATION, payload }
};
