import { user_login } from "./login";

const axios = require('axios');
let CONFIG = require('./../../lib/config.json')


export async function get_roaster_routes(route_id) {
	let action_token = localStorage.getItem('token')
	let base_url = CONFIG.server_api_url
	let resp = {}
	if(route_id == null) {
		return resp
	}

	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	await axios.get(base_url+'/common/roster/result/v1/'+route_id+'/', headers).then((response) => {
		let res = response.data
		if (res.status === "success"){
			console.log("no error while fetching roaster routes");
			resp = res.data
			//return res.data
		}else{
			console.log("error while fetching roaster routes");
		}
	});
	return resp
}
