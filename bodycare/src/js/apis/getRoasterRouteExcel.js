import { user_login } from "./login";

const axios = require('axios');
let CONFIG = require('./../../lib/config.json')


export async function get_roaster_routes_excel(route_id) {
	let action_token = localStorage.getItem('token')
	let base_url = CONFIG.server_api_url
	let resp = {}
	if(route_id == null) {
		return resp
	}

	let headers = {"responseType": "arraybuffer", "headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	await axios.get(base_url+'/common/roster/result/excel/v1/'+route_id+'/', headers).then((response) => {
		console.log(response);
		if (response.statusText === "OK"){
			console.log("no error while fetching roaster routes excel");
			const type = response.headers['content-type']
			const blob = new Blob([response.data], { type: type, encoding: 'UTF-8' })
			const link = document.createElement('a')
			link.href = window.URL.createObjectURL(blob)
			link.download = 'file.xlsx'
			link.click()
		}else{
			console.log("error while fetching roaster routes excel");
		}
	});
	return resp
}
