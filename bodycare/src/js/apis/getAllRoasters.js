const axios = require('axios');
let CONFIG = require('./../../lib/config.json')

export async function get_all_roaster() {
	//let user_token = await user_login()
	let action_token = localStorage.getItem('token')
	let base_url = CONFIG.server_api_url
	let resp = []

	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	await axios.get(base_url+'/common/cityroster/v1/1/', headers).then((response) => {
		let res = response.data
		if (res.status === "success"){
			console.log("no error while getting all roasters api");
			resp = res.data
		}else{
			console.log("error while getting all roasters api");
		}
	});
	return resp
}
