const axios = require('axios');
let CONFIG = require('./../../lib/config.json')

export async function create_roaster(params) {
	let action_token = localStorage.getItem('token');
	let base_url = CONFIG.server_api_url
	/*let params = {
		"warehouse_lat":"28.790923",
		"warehouse_lon":"77.343443",
		"name":"CItyROster",
		"slot_id":191119
	}*/
	console.log("params: ", params);
	let resp = {}
	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}

	await axios.post(base_url+'/common/cityroster/v1/1/', params, headers).then((response) => {
		let res = response.data
		if(res.status === "success") {
			console.log("no error while creating roaster", res);
			resp = res.data
		} else {
			//dispatch(accessoriesSell({sell: responseData}));
			//history.push("/dashboard")
			console.log("error while creating roaster");
		}
	});
	return resp
}
