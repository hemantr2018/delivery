const axios = require('axios');
let CONFIG = require('./../../lib/config.json')

export async function user_login(params) {
	//let action_token = JSON.parse(localStorage.getItem('token'));//"7d9f40a0d97cc7a78f8517e6bf60844e7435fbd42dea363fff5f278d80a950cb"
	//let base_url = "http://35.244.12.144:8005"
	let base_url = CONFIG.server_api_url
	/*let params = {
		"user_name":"himanshu",
		"password":"himanshu"
	}*/
	let resp = false;
	let headers = {"headers": {'Content-Type': 'application/json'}}

	await axios.post(base_url+'/users/tokens/v1/', params, headers).then((response) => {
		let res = response.data
		if(res.status === "success") {
			console.log("no error while login");
			localStorage.setItem('token', res.data.access_token);
			resp = true;
		} else {
			//dispatch(accessoriesSell({sell: responseData}));
			//history.push("/dashboard")
			console.log("error while login");
			resp = false;
		}
	});
	return resp;
}
