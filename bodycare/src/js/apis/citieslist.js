import { user_login } from "./login";

const axios = require('axios');
let CONFIG = require('./../../lib/config.json')


export async function fetch_city() {
	//let user_token = await user_login()
	let action_token = localStorage.getItem('token')
	//let base_url = "http://35.244.12.144:8005"
	let base_url = CONFIG.server_api_url
	let resp = {}

	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	await axios.get(base_url+'/common/cities/v1/', headers).then((response) => {
		let res = response.data
		if (res.status === "success"){
			resp = res.data
			console.log("no error while fetching city list api");
		}else{
			console.log("error while fetching city list api");
		}
	});
	return resp
}
