import { user_login } from "./login";

const axios = require('axios');
let CONFIG = require('./../../lib/config.json')


export async function get_grids() {
	//let user_token = await user_login()
	let action_token = localStorage.getItem('token')
	let base_url = CONFIG.server_api_url

	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	axios.get(base_url+'/common/citygeo/v1/1/', headers).then((response) => {
		let res = response.data
		if (res.status === "success"){
			console.log("no error while fetching grids api");
			return res.data
		}else{
			console.log("error while fetching grids api");
		}
	});
}
