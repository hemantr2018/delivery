import { user_login } from "./login";

const axios = require('axios');
let CONFIG = require('./../../lib/config.json')


export async function fetch_algos() {
	let action_token = localStorage.getItem('token')
	let base_url = CONFIG.server_api_url
	let resp = {}

	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	await axios.get(base_url+'/common/algorithms/v1/', headers).then((response) => {
		let res = response.data
		console.log("fetch_algos", res);
		if (res.status === "success"){
			resp = res.data
			console.log("no error while fetching city list api");
		}else{
			console.log("error while fetching city list api");
		}
	});
	return resp
}
