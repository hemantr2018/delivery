const axios = require('axios');
let CONFIG = require('./../../lib/config.json')

export async function create_roaster_orders(roaster_id, params) {
	let action_token = localStorage.getItem('token');
	let base_url = CONFIG.server_api_url
	/*let params =  [{
            "lat": "28.513438",
            "lon": "77.08019",
            "order_id":"HIMH_38",
            "item_count": 2
        
        },
        {
            "lat": "28.515438",
            "lon": "77.08119",
            "order_id":"HIMH_37",
            "item_count": 4
        
        }
    ]*/
	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	let resp = {}
	
	await axios.post(base_url+'/common/roster/orders/v1/'+roaster_id+'/', params, headers).then((response) => {
		let res = response.data
		if(res.status === "success") {
			console.log("no error while creating roaster orders", res);
			resp = res.status
		} else {
			//dispatch(accessoriesSell({sell: responseData}));
			//history.push("/dashboard")
			console.log("error while creating roaster orders");
		}
	});
	return resp;
}
