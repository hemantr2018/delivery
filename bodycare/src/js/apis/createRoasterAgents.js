const axios = require('axios');
let CONFIG = require('./../../lib/config.json')

export async function create_roaster_agents(roaster_id, params) {
	let action_token = localStorage.getItem('token');
	let base_url = CONFIG.server_api_url
	
	/*let params = [{
		"capacity": 22,
		"name": "Yogesh Chaubey",
		"area": "South and Central Delhi",
		"area_lat": "28.575501",
		"area_lon": "77.159577",
		"home_lat":"28.64317",
		"home_lon":"77.120388",
		"working_hours": 10
		},
		{
		 "capacity": 20,
			"name": "Vijay",
			"area": "Nirman vihar,shastri park, azad nagar and govind pura",
			"start_grid_id": 1119,
			"grid_id": 1187,
			"area_lat": "28.670283",
			"area_lon":"77.263859",
			"home_lat":"28.695408",
			"home_lon":"77.311613",
			"working_hours": 10,
			"end_location": "28.695408, 77.311613"
		}
    ]*/
	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	let resp = {}

	await axios.post(base_url+'/common/roster/agents/v1/'+roaster_id+'/', params, headers).then((response) => {
		let res = response.data
		if(res.status === "success") {
			console.log("no error while creating roaster agents", res);
			resp = res.status
		} else {
			//dispatch(accessoriesSell({sell: responseData}));
			//history.push("/dashboard")
			console.log("error while creating roaster agents");
		}
	});
	
	return resp;
}
