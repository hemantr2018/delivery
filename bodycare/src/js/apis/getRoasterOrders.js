const axios = require('axios');
let CONFIG = require('./../../lib/config.json')

export async function get_roaster_orders(roaster_id) {
	//let user_token = await user_login()
	let action_token = localStorage.getItem('token')
	let base_url = CONFIG.server_api_url
	let resp = {}
	
	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	await axios.get(base_url+'/common/roster/orders/v1/'+roaster_id+'/', headers).then((response) => {
		let res = response.data
		if (res.status === "success") {
			resp = res.data
			console.log("no error while getting all roaster orders");
		} else {
			console.log("error while getting all roaster orders");
		}
	});
	return resp;
}
