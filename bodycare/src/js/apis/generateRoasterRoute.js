const axios = require('axios');
let CONFIG = require('./../../lib/config.json')

export async function generate_roaster_route(roaster_id, params=null) {
	let action_token = localStorage.getItem('token');
	let base_url = CONFIG.server_api_url
	let headers = {"headers": {'Content-Type': 'application/json', 'access-token': action_token}}
	let resp = false
	
	await axios.post(base_url+'/common/roster/result/v1/'+roaster_id+'/', params, headers).then((response) => {
		let res = response.data
		if(res.status === "success") {
			resp = true
			console.log("no error while creating roaster orders", res);
		} else {
			//dispatch(accessoriesSell({sell: responseData}));
			//history.push("/dashboard")
			console.log("error while creating roaster orders");
		}
	});
	return resp;
}
